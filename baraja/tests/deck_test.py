""" unittests for the Deck class

"""
import unittest
from baraja.card import Card
from baraja.deck import Deck
from baraja.deck import PlayingCardDeck
from baraja.exceptions import DeckException


class DeckTests(unittest.TestCase):
    def setUp(self):
        self.cardlist = [Card('One', 1),
                         Card('Two', 2)]
        self.deck = Deck(self.cardlist)

    def testDraw(self):
        """Deck draw"""
        card = self.deck.draw()
        self.assertTrue(card in self.cardlist)
        self.assertEqual(1, len(self.deck.drawn))
        self.assertEqual(1, len(self.deck.undrawn))

    def testDrawDictValue(self):
        """Draw a card with rich data, confirm copy"""
        cardlist = Card('monster', {'attack': 1, 'health': 1}) * 2
        deck = Deck(cardlist)
        card = deck.draw()
        self.assertEqual(1, len(deck.drawn))
        card.value['attack'] = 2
        self.assertNotEqual(card.value['attack'], deck.drawn[0].value['attack'])

    def testEmptyDraw(self):
        """Deck empty draw"""
        self.deck.draw()
        self.deck.draw()
        self.assertEqual(None, self.deck.draw())

    def testShuffle(self):
        """Deck shuffle"""
        self.deck.add(Card('Three', 3))
        self.deck.draw()
        self.assertEqual(2, len(self.deck.undrawn))
        self.assertEqual(1, len(self.deck.drawn))
        self.assertEqual(0, len(self.deck.discarded))
        self.deck.shuffle()
        self.assertEqual(2, len(self.deck.undrawn))
        self.assertEqual(1, len(self.deck.drawn))
        self.assertEqual(0, len(self.deck.discarded))
        self.deck.discard(self.deck.drawn[0])
        self.deck.shuffle()
        self.assertEqual(3, len(self.deck.undrawn))
        self.assertEqual(0, len(self.deck.drawn))
        self.assertEqual(0, len(self.deck.discarded))
        self.deck.draw()
        self.deck.discard(self.deck.drawn[0])
        self.deck.shuffle(discarded=False)
        self.assertEqual(2, len(self.deck.undrawn))
        self.assertEqual(0, len(self.deck.drawn))
        self.assertEqual(1, len(self.deck.discarded))

    def testRemove(self):
        """Remove a card"""
        deck = Deck(Card('One', 1) * 10)
        c1 = deck.draw()
        # remove from drawn
        c2 = deck.remove(c1)
        # make sure it is found
        self.assertIsNotNone(c2)
        # make sure it doesn't get found again
        c2 = deck.remove(c1)
        self.assertIsNone(c2)
        # draw a card and shuffle to test undrawn
        c1 = deck.draw()
        deck.shuffle()
        c2 = deck.remove(c1)
        # make sure it is found
        self.assertIsNotNone(c2)
        # make sure it doesn't get found again
        c2 = deck.remove(c1)
        self.assertIsNone(c2)
        # make sure discarded can be removed
        c1 = deck.draw()
        deck.discard(c1)
        c2 = deck.remove(c1)
        self.assertIsNotNone(c2)

    def testAdd(self):
        """Add a card"""
        card = Card('Three', 3)
        # card should be added to undrawn
        self.deck.add(card)
        self.assertEqual(len(self.deck.undrawn), 3)
        # should on bottom of deck, not drawn first
        self.assertNotEqual(self.deck.draw(), card)
        # should raise error on duplicate uuid add
        with self.assertRaises(DeckException):
            self.deck.add(card)
        # length of deck is still 3, ensure add didn't occur
        self.assertEqual(len(self.deck.undrawn) + len(self.deck.drawn), 3)
        # add should check drawn and undrawn
        self.deck.draw()
        self.deck.draw()
        with self.assertRaises(DeckException):
            self.deck.add(card)

    def testDiscard(self):
        """Discard a card"""
        # new deck has no discarded
        self.assertEqual(0, len(self.deck.discarded))
        card = self.deck.draw()
        # discard should move card
        self.deck.discard(card)
        self.assertEqual(1, len(self.deck.discarded))
        # make sure it is the same card
        self.assertEqual(card.uuid, self.deck.discarded[0].uuid)
        # trying to remove the wrong card raises an error
        with self.assertRaises(DeckException):
            self.deck.discard(card)

    def testSize(self):
        '''Size of deck'''
        # initial deck size
        self.assertEqual(2, self.deck.size)
        # drawn and undrawn are included
        self.deck.draw()
        self.assertEqual(2, self.deck.size)


class PlayingCardDeckTests(unittest.TestCase):
    def setUp(self):
        self.deck = PlayingCardDeck()

    def testCreation(self):
        """PlayingDeck creation"""
        self.assertEqual(52, len(self.deck.undrawn))

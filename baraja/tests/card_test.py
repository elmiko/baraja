""" unittests for the Card class

"""
import copy
import unittest

from baraja.card import Card
from baraja.card import PlayingCard
from baraja.exceptions import CardException


class CardTests(unittest.TestCase):
    def setUp(self):
        self.cardone = Card('One', 1)

    def testRepr(self):
        """Card repr"""
        self.assertEqual("baraja.card.Card('One', 1)",
                         repr(self.cardone))

    def testEq(self):
        """Card equality"""
        c = Card('One', 1)
        self.assertTrue(self.cardone == c)

    def testEqNoValue(self):
        """Card equality"""
        c1 = Card('One')
        c2 = Card('One')
        self.assertTrue(c1 == c2)

    def testMulList(self):
        cardlist = self.cardone * 2
        self.assertTrue(len(cardlist) == 2)
        for c in cardlist:
            self.assertEqual(self.cardone, c)

    def testName(self):
        """Card name"""
        self.assertEqual('One', self.cardone.name)

    def testNe(self):
        """Card inequality"""
        c = Card('Two', 2)
        self.assertTrue(self.cardone != c)

    def testValue(self):
        """Card value"""
        self.assertEqual(1, self.cardone.value)

    def testUuidNe(self):
        """Card Uuid inequality"""
        c = Card('One', 1)
        self.assertTrue(self.cardone.uuid != c.uuid)

    def testUuidEq(self):
        """Card Uuid equality"""
        c = copy.deepcopy(self.cardone)
        self.assertEqual(c.uuid, self.cardone.uuid)

    def testMulListUuidUniq(self):
        """Card multiplier creates unique cards"""
        cardlist = self.cardone * 2
        self.assertNotEqual(cardlist[0].uuid, cardlist[1].uuid)

    def testCopy(self):
        """Card copy is unique but has same values"""
        c = self.cardone.copy()
        self.assertNotEqual(c.uuid, self.cardone.uuid)
        self.assertEqual(c, self.cardone)

    def testClone(self):
        """Card clone is exact duplicate"""
        c = self.cardone.clone()
        self.assertEqual(c.uuid, self.cardone.uuid)
        self.assertEqual(c, self.cardone)


class PlayingCardTests(unittest.TestCase):
    def setUp(self):
        self.aceofhearts = PlayingCard(1, PlayingCard.HEART)

    def testEq(self):
        """PlayingCard equality"""
        c = PlayingCard(1, PlayingCard.HEART)
        self.assertTrue(self.aceofhearts == c)

    def testName(self):
        """PlayingCard name"""
        self.assertEqual('Ace of Hearts', self.aceofhearts.name)

    def testNe(self):
        """PlayingCard inequality"""
        c = Card(2, PlayingCard.HEART)
        self.assertTrue(self.aceofhearts != c)
        c = Card(1, PlayingCard.CLUB)
        self.assertTrue(self.aceofhearts != c)
        c = Card(2, PlayingCard.CLUB)
        self.assertTrue(self.aceofhearts != c)

    def testSuit(self):
        """PlayingCard suit"""
        with self.assertRaises(CardException):
            PlayingCard(1, 'Spam')

    def testValue(self):
        """PlayingCard value"""
        with self.assertRaises(CardException):
            PlayingCard(0, PlayingCard.HEART)

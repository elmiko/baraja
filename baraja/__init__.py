"""__init__.py

baraja deck manipulation module

"""

__all__ = ['card', 'deck']

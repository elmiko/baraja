"""baraja.exceptions

all the exceptions for baraja

"""


class CardException(Exception):
    """Any error produced by a Card class"""
    pass


class DeckException(Exception):
    """Any error produced by a Deck class"""
    pass

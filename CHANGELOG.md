# Changelog

## devel

## release-0.3.0
* add Deck.size property to show total deck size
* Deck.add now properly checks drawn and undrawn for duplicates

## release-0.2.0
* add discard function to Deck
* new Decks no longer shuffle on create
* add clone function to Card
* add remove function to Deck
* add uuid property to Cards
* add copy function to Card
